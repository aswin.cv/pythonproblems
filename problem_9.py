/*
Name of the project : Python Problems
File name : python_9.py
Description : Write a program that calculates and prints the value according
              to the given formula:Q = Square root of [(2 C D)/H]
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_9.py
Input : 100,150
Output : 18,22
*/


import math


def find(d):
    c,h = 50,30
    result = int(math.sqrt( (2 * c * int(d))/h))
    print(result)

str = input("enter the input   :  ")
str1 = str.split(",")
for i in range(0,len(str1)) :
     find(str1[i])
print("")

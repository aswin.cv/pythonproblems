/*
Name of the project : Python Problems
File name : python_8.py
Description : define a class with atleast 2 methods.getString and printString.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_8.py
Input : hello
Output : HELLO
*/


class out(object):
    def __init__(self):
        self.str = self.getString()
    def getString(self):
        return(input("enter the string :"))
    def printString(self):
        print((self.str).upper())
obj = out()
obj.printString()

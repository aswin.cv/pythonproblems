/*
Name of the project : Python Problems
File name : python_10.py
Description : program which takes 2 digits, X,Y as input and generates a
              2-dimensional array. The element value in the i-th row and j-th
              column of the array should be i*j.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_10.py
Input : 5,5
Output : [[0, 0, 0, 0, 0], [0, 1, 2, 3, 4], [0, 2, 4, 6, 8], [0, 3, 6, 9, 12],
         [0, 4, 8, 12, 16]]
*/


def solve(m,n):
    arr = [[0 for x in range(m)] for y in range(n)]
    for i in range(0,m) :
        for j in range(0,n) :
            arr[i][j] = i*j
    return arr
num1 = input("enter the value of m  :  ")
num2 = input("enter the value of n  :  ")
print(solve(int(num1),int(num2)))

/*
Name of the project : Python Problems
File name : python_6.py
Description : to create a dictionary with square values up to given limit
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_6.py
Input : 5
Output : {1: 1, 2: 4, 3: 9, 4: 16, 5: 25}
*/


def square(num) :
    out = {}
    for i in range(1,num+1) :
        out[i] = i*i
        i += 1
    return out
numb = input("enter the number     :")
print("")
print(square(int(numb)))
